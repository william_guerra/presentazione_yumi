# Appunti del corso di programmazione YuMi
***
Quello che segue � il repository contenente le slides del corso di programmazione del robot collaborativo YuMi di ABB. 

Nel repository sono presenti, sotto la cartella samples, alcuni esempi di codice liberamete usabili nella
programmazione.

Per il download dell'ultima versione di tutti i file si rimanda al seguente link [Latest
version](https://bitbucket.org/william_guerra/presentazione_yumi/downloads/?tab=branches)
