MODULE trap
    !
    ! ************************************************************************
    ! * Programma di esempio di uso di procedure trap (interrupt)
    ! * 
    ! * Autore: William Guerra <william.guerra@mr-robotica.it>
    ! * Ditta : MR Robotica
    ! *
    ! * Link  : https://www.bitbucket.org/william_guerra/presentazione_yumi.git
    ! ************************************************************************
    ! 
    ! Modulo di esempio per la creazione e la connessione delle routine Trap ad eventi.
    !
    ! Nel modulo ho riportato alcuni esempi di definizione di routine trap e il modo per connetterle ad eventi quali il
    ! cambiamento di stato di segnali digitali. 
    !
    ! Le routine trap si dimostrano particolarmente efficaci quando è necessario eseguire determinate azioni solo al
    ! verificarsi di alcuni eventi durante l'esecuzione del ciclo di produzione e non si vuole o non si può inserire un
    ! controllo del verificarsi di tale evento nel codice. 
    !
    ! Ad esempio: Il robot usa un sistema di vuoto per agganciare l'oggetto in lavorazione e si vuole supervisionare lo
    ! stato del vacuostato in modo da intercettare un'eventuale perdita durante il ciclo.
    !
    VAR intnum vacuumTrap;

    ! Alias di segnale IO
    VAR signaldi DI_VACUUM_OK;

    TRAP vacuumLost
        ! Routine trap per gestire la perdita di vuoto durante il ciclo di lavoro
        ! La routine arresta i movimenti del robot, mostra un messaggio sulla flex pendant e mette in pausa il
        ! programma, alla ripresa del programma controlla che il vuoto sia stato raggiunto prima di riavviare i
        ! movimenti altrimenti il programma viene rimesso in pausa
        StopMove;
        
        WHILE DI_VACUUM_OK = 0 DO
            TPErase;
            TPWrite "ATTENZIONE!";
            TPWrite "Perdita di vuoto sulla pinza";
            Stop;
        ENDWHILE
        
        StartMove;
    ENDTRAP

    PROC initTrap()
        ! Routine di inizializzazione dell'interrupt, la procedura dev'essere lanciata ad ogni avvio del programma in
        ! quanto la variabile vacuumLost viene resettata ad ogni spostamento del PP

        ! Prima di procedere con l'assegnazione della routine trap è bene eliminare eventuali vecchie connessioni
        ! dell'interrupt in modo da non ricevere errori durante la connessione
        IDelete vacuumLost;
        !
        ! Connessione della routine alla variabile interrupt, è possibile collegare più variabili interrupt alla stessa
        ! routine ma non il contrario
        CONNECT vacuumLost WITH vacuumTrap;
        !
        ! Definizione della condizione di trigger della procedura di interrupt
        ISignalDI vacuumLost, low, DI_VACUUM_OK;
        !
        ! Abilitazione degli interrupt
        IEnable;
        IWatch vacuumLost;
    ENDPROC

    PROC ciclo()
        !
        ! Durante l'esecuzione del programma è possibile disabilitare e riattivare l'interrupt usando
        !
        ! Disabilita temporaneamente l'interrupt, eventuali eventi di interrupt vengono ignorati
        ISleep vacuumLost;
        !
        ! Riattivazione dell'interrupt
        IWatch vacuumLost;
        !
        ! Disabilitazione di tutti gli interrupt.
        ! NOTA: In questo caso eventuali eventi di interrupt non vengono ignorati ma messi in coda ed eseguiti quando
        ! vengono riattivati gli interrupt
        IDisable;
        !
        ! Riabilitazione degli interrupt
        IEnable;

    ENDPROC
