MODULE advanced_proc
    !
    ! ************************************************************************
    ! * Programma esempio di passaggio dati avanzato
    ! * 
    ! * Autore: William Guerra <william.guerra@mr-robotica.it>
    ! * Ditta : MR Robotica
    ! *
    ! * Link  : https://www.bitbucket.org/william_guerra/presentazione_yumi.git
    ! ************************************************************************
    !
    ! In questo modulo riporto alcuni esempi di passaggio avanzato di parametri alle procedure. Tali metodi non sono da
    ! considerarsi obbligatori, possono essere utili in alcune circostanze per ridurre la complessità del codice o la
    ! riusabilità di parti di esso.
    !
    PROC example()
        !
    ENDPROC

    PROC swap_1(num numero1, num numero2)
        ! La procedura esegue, o tenta di eseguire, lo swap dei due dati passati alla procedura al momento della sua
        ! chiamata. 
        !
        ! i dati numero1 e numero2 sono delle copie dei dati passati al momento della chiamata della procedura, la
        ! modifica dei dati all'interno della procedura, pertanto, avviene su questa copia senza essere propagata ai
        ! dati della procedura chiamante.
        !
        ! Il risultato finale è quindi una funzione inutile
        VAR num tmp;

        tmp := numero1;
        numero1 := numero2;
        numero2 := tmp;
    ENDPROC

    PROC swap_2(INOUT num numero1, INOUT num numero2)
        ! La procedura esegue lo swap dei due numeri passati alla procedura al momento della sua chiamata. 
        !
        ! Questa procedura, a differenza della swap_1, funziona in quanto l'attributo INOUT comporta il passaggio non di
        ! una copia del dato, bensì viene passato un riferimento al dato stesso. In altre parole è un modo per
        ! permettere alla procedura di accedere direttamente al dato della procedura chiamante
        VAR num tmp;

        tmp := numero1;
        numero1 := numero2;
        numero2 := tmp;
    ENDPROC

    PROC swap_3(PERS num numero1, PERS num numero2)
        VAR num tmp;

        tmp := numero1;
        numero1 := numero2;
        numero2 := tmp;
    ENDPROC
ENDMODULE
