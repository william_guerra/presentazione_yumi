MODULE camera
    !
    !
    ! ************************************************************************
    ! * Programma esempio di controllo della telecamera
    ! * 
    ! * Autore: William Guerra <william.guerra@mr-robotica.it>
    ! * Ditta : MR Robotica
    ! *
    ! * Link  : https://www.bitbucket.org/william_guerra/presentazione_yumi.git
    ! ************************************************************************
    !
    ! Nel programma si fa riferimento a CamExt, questo è un dato di tipo cameradev ed è predefinito nei task (non serve
    ! definirlo o inizializzarlo) e prende il nome della telecamera configurata
    !
    ! Variabile per la memorizzazione del risultato proveniente dalla telecamera
    VAR cameratarget camResult;

    PERS wobjdata wPianoPrelievo := [FALSE,TRUE,"",[[319.887,1.30863,57],[0.999983,0.00274151,0.00261859,0.00445149]],[[17.9664, -16.7223, 0],[0.182933,0,0,-0.983125]]];

    PERS robtarget zero := [[0, 0, 0],[1, 0, 0, 0],[0,-3,-2,4],[-82.1451,9E+09,9E+09,9E+09,9E+09,9E+09]];

    PERS tooldata pinza := [TRUE,[[0,0,130],[1,0,0,0]],[0.3,[0,0,50],[1,0,0,0],0.00021,0.00024,9E-05]];

    PROC ricerca_pezzo()
        !
        ! Procedura per la ricerca di un pezzo tramite sistema di visione integrato e movimento di prelievo con il dato
        ! appena trovato.
        !
        ! NOTA: L'istruzione CamGetResult estrae i dati dall'array dei risultati senza eliminare il dato, se sono
        ! necessari i risultati successivi è necessario scorrere l'array richiamando CamGetResult
        !
        ! Controllo se sono presenti pezzi da prelevare
        WHILE (CamNumberOfResults(CamExt) <= 0) DO
            ! No, richiedo una nuova immagine
            CamFlush CamExt;
            CamReqImage CamExt;
        ENDWHILE
        !
        ! Sono presenti dei pezzi, salvo i dati delle posizioni dei singoli pezzi
        CamGetResult CamExt, camResult \MaxTime := 2;
        !
        ! Aggiorno l'oframe del piano di prelievo con il risultato ottenuto dalla telecamera
        wPianoPrelievo.oframe := camResult.cframe;
        !
        ! Avvicinamento al punto di prelievo e prelievo del pezzo
        MoveL Offs(zero, 0, 0, 20), v400, z100, pinza, \WObj := wPianoPrelievo;
        MoveL zero, v100, fine, pinza, \WObj := wPianoPrelievo;
        !
        ! Prelievo del pezzo
        !
        MoveL Offs(zero, 0, 0, 20), v400, z100, pinza, \WObj := wPianoPrelievo;
    ENDPROC

    PROC carica_job()
        ! Per caricare un job nella telecamera è necessario prima impostare la telecamera in modalità programmazione
        CamSetProgramMode CamExt;
        ! Richiesta di caricamento del job myJob.job alla telecamera CamExt, il programma attende il caricamento, con
        ! l'opzione /MaxTime si inserisce un timeout di attesa (da gestire con routine di errore)
        CamLoadJob CamExt, "myJob.job";
        ! Imposta la telecamera in modalità Run per avviare il job appena caricato
        CamSetRunMode CamExt;

    ERROR
        ! Se la telecamera sta eseguendo un job potrebbe essere ancora in esecuzione al momento del cambio del job, in
        ! quel caso si attende che il job venga terminato e si ritenta
        IF ERRNO = ERR_CAM_BUSY THEN
            WaitTime 1;
            RETRY;
        ENDIF
    ENDPROC
ENDMODULE
