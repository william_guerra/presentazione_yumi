MODULE smart_gripper
    !
    ! ************************************************************************
    ! * Programma esempio di gestione delle smartgripper
    ! * 
    ! * Autore: William Guerra <william.guerra@mr-robotica.it>
    ! * Ditta : MR Robotica
    ! *
    ! * Link  : https://www.bitbucket.org/william_guerra/presentazione_yumi.git
    ! ************************************************************************
    ! Modulo con esempi di implementazione di procedure e funzioni di gestione delle smart gripper
    ! Per una lista completa delle procedure e delle funzioni messe a disposizione dalla SmartGripper rimando al
    ! documento:
    !         Manuale del prodotto - Pinze per IRB14000
    !
    ! Nel modulo vengono usate le funzioni più recenti, ovvero quelle nella forma g_<procedura>, è possibile usare anche
    ! le procedure obsolete, quelle che iniziano con Hand<procedura>. In questo caso viene mostrato un warning all'avvio
    ! del programma.
    !
    !
    ! Procedura per la calibrazione del modulo servo assistito della SmartGripper
    PROC calibra_gripper()
        ! Controllo se il modulo è scalibrato
        IF (NOT g_IsCalibrated()) THEN 
            ! Il modulo è scalibrato, inizio la sequenza di calibrazione, la doppia calibrazione che
            ! viene eseguita è necessaria in quanto alcune volte il comando di chiusura pinza con il modulo scalibrato
            ! non chiude completamente le griffe.
            g_JogOut;
            g_JogIn;
            g_Calibrate;
            g_JogIn;
            g_Calibrate;
        ENDIF
    ENDPROC

    PROC muovi_pinza()
        ! Comando di apertura della pinza
        g_JogIn;
        ! Comando di chusura della pinza
        g_JogOut;
        ! Comando di spostamento della pinza alla distanza di 10mm dalla posizione di calibrazione
        g_MoveTo 10;
        g_MoveTo 15;
        g_MoveTo 20;
        ! Comando di tentativo di afferrare l'oggetto chiudendo la pinza
        g_GripIn;
    ENDPROC

    PROC lettura_pinza()
        VAR num lettura;

        ! Lettura delle informazioni correnti della pinza:
        !
        ! Posizione della pinza
        lettura := g_GetPos();
        ! Velocità della pinza
        lettura := g_GetSpd();
        ! Stato della pinza
        lettura := g_GetStatus();
    ENDPROC

    PROC comandi_pneumatici()
        ! Attivazione e disattivazione del soffio sul modulo pneumatico 1
        g_BlowOn1;
        g_BlowOff1;

        ! Attivazione e disattivazione del vuoto sul modulo pneumatico 1
        g_VacuumOn1;
        g_VacuumOff1;
    ENDPROC
ENDMODULE
