MODULE rapid_flow_control
    !
    ! ************************************************************************
    ! * Programma di esempio di controllo del flusso del programma semplificato
    ! * 
    ! * Autore: William Guerra <william.guerra@mr-robotica.it>
    ! * Ditta : MR Robotica
    ! *
    ! * Link  : https://www.bitbucket.org/william_guerra/presentazione_yumi.git
    ! ************************************************************************
    !
    ! In questo modulo riporto alcuni esempi dei costrutti di base per il controllo del flusso del programma in
    ! un programma RAPID. Questo modulo contiene lo stesso programma in rapid_flow_control.mod, con la differenza che
    ! sono stati eliminate alcune parti non strettamente necessarie al movimento vero e proprio in modo da enfatizzare
    ! il solo approccio al problema.
    !
    ! In questo caso ho voluto creare un piccolo programma di pallettizzazione con ritorno in posizione di home
    ! all'avvio del ciclo. Prelievo da un punto prestabilito e deposito in un pallet con N righe, M colonne e S strati
    ! di deposito.
    !
    PERS robtarget prelievo := [[7.43,-373.82,221.17],[0.0169564,0.682861,0.730337,0.00466253],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget deposito := [[359.69,-2.37,221.17],[0.00890056,-0.019954,0.999646,0.015167],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget overPallet := [[359.69,-2.37,294.53],[0.0089006,-0.019954,0.999646,0.015167],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    !
    PERS jointtarget home := [[-2.0044,0.0392347,-0.0392366,3.46678,30.0607,-4.00391],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    !
    PERS wobjdata pallet := [FALSE,TRUE,"",[[0,0,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
    !
    PERS tooldata pinza:=[TRUE,[[0,0,100],[1,0,0,0]],[1,[0,0,50],[1,0,0,0],0,0,0]];
    !
    PERS num righeMax := 3;
    PERS num colonneMax := 3;
    PERS num riga := 0;
    PERS num colonna := 0;
    PERS num passoX := 42;
    PERS num passoY := 84;
    !
    PROC main()
        ! Reset degli override di velocità e accelerazione del robot
        VelSet 100, MaxRobSpeed();
        AccSet 100, 100;
        !
        ! Reset dei segnali e della pinza
        resetSegnali;
        !
        ! Attesa della richiesta di ritorno in posizione di home da parte del PLC
        WaitTime 1;
        moveToHome;
        !
        FOR riga FROM 0 TO righeMax DO 
            FOR colonna FROM 0 TO colonneMax DO 
                !
                ! Avvicinamento alla posizione di prelievo
                MoveJ Offs(prelievo, 0, 0, 50), v1000, z100, pinza;
                !
                ! Simulazione di attesa del consenso al prelievo del pezzo
                WaitTime 1;
                !
                ! Apertura della pinza e movimento il posizione di prelievo
                movePinza \Open;
                MoveL prelievo, v300, fine, pinza;
                !
                ! Chiusura della pinza (e attesa che il pezzo si stabilizzi nella pinza) e segnalazione di prelievo effettuato
                movePinza \Close;
                WaitTime \InPos, 0.5;
                !
                ! Allontanamento dal punto di prelievo e avvicinamento al pallet di deposito, il robot si muove spostandosi nei
                ! punti usati per il prelievo con ordine inverso
                MoveJ Offs(prelievo, 0, 0, 50), v1000, z100, pinza;
                MoveAbsJ home, v1000, z100, tool0;
                MoveJ overPallet, v1000, z100, pinza;
                !
                ! Simulazione di attesa del consenso al deposito sul pallet
                WaitTime 1;
                !
                ! Avvicinamento alla pila di deposito e deposito sopra alla pila
                MoveJ Offs(deposito, passoX * riga, passoY * colonna, 50), v1000, z100, pinza, \WObj := pallet;
                MoveL Offs(deposito, passoX * riga, passoY * colonna, 0), v100, fine, pinza, \WObj := pallet;
                !
                ! Apertura della pinza, attesa di caduta del pezzo e segnalazione di pezzo depositato
                movePinza \Open;
                WaitTime \InPos, 0.5;
                !
                ! Allontanamento dal punto di deposito e ritorno in posizione di home
                MoveJ Offs(deposito, passoX * riga, passoY * colonna, 50), v1000, z100, pinza, \WObj := pallet;
                MoveJ overPallet, v1000, z100, pinza;
                MoveAbsJ home, v1000, z100, tool0;
            ENDFOR
        ENDFOR
        ! 
        ! Pallet terminato, il robot si trova in home e attendo che l'operatore scarichi il pallet
        TPErase;
        TPWrite "Pallet terminato, premere start";
        Stop;
        RETURN;
    ENDPROC

    PROC resetSegnali()
        !
        ! Inizializzazione della SmartGripper
        g_Init \holdForce := 15;
        ! 
        ! Controllo se la SmartGripper richiede una calibrazione. Se non richiesta procedo con il resto della procedura
        IF NOT g_IsCalibrated THEN
            ! Calibrazione della pinza, la doppia calibrazione è necessaria in quanto alcune volte la SmartGripper non si
            ! chiude del tutto quando la pinza è scalibrata
            g_JogIn;
            g_Calibrate;
            j_JogIn;
            g_Calibrate;
        ENDIF
        !
        ! Dopo aver calibrato la pinza apri le griffe
        movePinza \Open;
        RETURN;
    ENDPROC

    PROC moveToHome()
        ! Procedura per il ritorno nella posizione di home
        MoveAbsJ home, v300, fine, pinza;
        RETURN;
    ENDPROC

    PROC movePinza(\switch Open | switch Close)
        ! Procedura per la gestione della pinza (apertura e chiusura)
        IF (Present(Open)) THEN
            g_JogOut;
        ELSEIF (Present(Close)) THEN
            g_GripIn \holdForce := 15;
        ENDIF
        RETURN;
    ENDPROC
ENDMODULE
