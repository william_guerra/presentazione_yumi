MODULE rapid_flow_control
    !
    ! ************************************************************************
    ! * Programma di esempio di controllo del flusso del programma
    ! * 
    ! * Autore: William Guerra <william.guerra@mr-robotica.it>
    ! * Ditta : MR Robotica
    ! *
    ! * Link  : https://www.bitbucket.org/william_guerra/presentazione_yumi.git
    ! ************************************************************************
    !
    ! In questo modulo riporto alcuni esempi dei costrutti di base per il controllo del flusso del programma in
    ! un programma RAPID.
    !
    ! In questo caso ho voluto creare un piccolo programma di pallettizzazione con ritorno in posizione di home
    ! all'avvio del ciclo. Prelievo da un punto prestabilito e deposito in un pallet con N righe, M colonne e S strati
    ! di deposito
    !
    !PERS robtarget home := [[482.31,164.08,330.70],[0.107017,0.0431841,-0.981889,0.150255],[0,0,0,4],[-110.955,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget prelievo := [[7.43,-373.82,221.17],[0.0169564,0.682861,0.730337,0.00466253],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget deposito := [[359.69,-2.37,221.17],[0.00890056,-0.019954,0.999646,0.015167],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    PERS robtarget overPallet := [[359.69,-2.37,294.53],[0.0089006,-0.019954,0.999646,0.015167],[-1,0,-1,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    !
    PERS jointtarget home := [[-2.0044,0.0392347,-0.0392366,3.46678,30.0607,-4.00391],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]];
    !
    PERS wobjdata pallet := [FALSE,TRUE,"",[[0,0,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
    !
    PERS tooldata pinza:=[TRUE,[[0,0,100],[1,0,0,0]],[1,[0,0,50],[1,0,0,0],0,0,0]];
    !
    PERS num righeMax := 3;
    PERS num colonneMax := 3;
    PERS num riga := 0;
    PERS num colonna := 0;
    PERS num passoX := 42;
    PERS num passoY := 84;
    !
    VAR signaldi DI_REQ_HOME;
    VAR signaldi DI_PRELIEVO;
    VAR signaldi DI_DEPOSITO;
    VAR signaldi DI_GRP_APERTA;
    VAR signaldi DI_GRP_CHIUSA;
    !
    VAR signaldo DO_ROB_HOME;
    VAR signaldo DO_PRELIEVO_OK;
    VAR signaldo DO_DEPOSITO_OK;
    VAR signaldo DO_OPEN_GRP;
    VAR signaldo DO_CLOSE_GRP;
    !
    PERS bool SIM_ON := FALSE;
    !
    PROC main()
        !
        ! Reset delle impostazioni di controllo della supervisione della configurazione degli assi e del comportamento
        ! del robot vicino ai punti di singolarità. 
        ! Queste impostazioni permettono il rilassamento della supervisione dell'orientamento del tool. Il percorso che
        ! il robot segue a velocità differenti potrebbe differire vistosamente. 
        !
        ! Secondo il mio parere conviene usarle il meno possibile e solo quando strettamente necessario
        ConfL \On;
        ConfJ \On;
        SingArea \Off;
        !
        ! Reset degli override di velocità e accelerazione del robot
        VelSet 100, MaxRobSpeed();
        AccSet 100, 100;
        !
        ! Procedura di reset dei segnali scambiati con il PLC
        resetSegnali; 
        !
        ! Attesa della richiesta di ritorno in posizione di home da parte del PLC
        IF (NOT SIM_ON) WaitDI DI_REQ_HOME, 1;
        IF (SIM_ON) WaitTime 1;
        moveToHome;
        !
        FOR riga FROM 0 TO righeMax DO 
            FOR colonna FROM 0 TO colonneMax DO 
                !
                ! Avvicinamento alla posizione di prelievo
                MoveJ Offs(prelievo, 0, 0, 50), v1000, z100, pinza;
                !
                ! Attesa del consenso al prelievo del pezzo
                IF (NOT SIM_ON) WaitDI DI_PRELIEVO, 1;
                IF (SIM_ON) WaitTime 1;
                Reset DO_DEPOSITO_OK;
                !
                ! Apertura della pinza e movimento il posizione di prelievo
                movePinza \Open;
                MoveL prelievo, v300, fine, pinza;
                !
                ! Chiusura della pinza (e attesa che il pezzo si stabilizzi nella pinza) e segnalazione di prelievo effettuato
                movePinza \Close;
                WaitTime \InPos, 0.5;
                Set DO_PRELIEVO_OK;
                !
                ! Allontanamento dal punto di prelievo e avvicinamento al pallet di deposito, il robot si muove spostandosi nei
                ! punti usati per il prelievo con ordine inverso
                MoveJ Offs(prelievo, 0, 0, 50), v1000, z100, pinza;
                MoveAbsJ home, v1000, z100, tool0;
                MoveJ overPallet, v1000, z100, pinza;
                !
                ! Attesa al consenso al deposito del pezzo 
                IF (NOT SIM_ON) WaitDI DI_DEPOSITO, 1;
                IF (SIM_ON) WaitTime 1;
                Reset DO_PRELIEVO_OK;
                !
                ! Avvicinamento alla pila di deposito e deposito sopra alla pila
                MoveJ Offs(deposito, passoX * riga, passoY * colonna, 50), v1000, z100, pinza, \WObj := pallet;
                MoveL Offs(deposito, passoX * riga, passoY * colonna, 0), v100, fine, pinza, \WObj := pallet;
                !
                ! Apertura della pinza, attesa di caduta del pezzo e segnalazione di pezzo depositato
                movePinza \Open;
                WaitTime \InPos, 0.5;
                Set DO_DEPOSITO_OK;
                !
                ! Allontanamento dal punto di deposito e ritorno in posizione di home
                MoveJ Offs(deposito, passoX * riga, passoY * colonna, 50), v1000, z100, pinza, \WObj := pallet;
                MoveJ overPallet, v1000, z100, pinza;
                MoveAbsJ home, v1000, z100, tool0;
            ENDFOR
        ENDFOR
        ! 
        ! Pallet terminato, il robot si trova in home e attendo che l'operatore scarichi il pallet
        TPErase;
        TPWrite "Pallet terminato, premere start";
        Stop;
        RETURN;
    ENDPROC

    PROC resetSegnali()
        ! Procedura per resettare i segnali eventualmente attivi durante la fase di riassetto dell'isola
        Reset DO_ROB_HOME;
        Reset DO_PRELIEVO_OK;
        Reset DO_DEPOSITO_OK;
        ! Apertura della pinza
        movePinza \Open;
        RETURN;
    ENDPROC

    PROC moveToHome()
        ! Procedura per il ritorno nella posizione di home
        MoveAbsJ home, v300, fine, pinza;
        RETURN;
    ENDPROC

    PROC movePinza(\switch Open | switch Close)
        ! Procedura per la gestione della pinza (apertura e chiusura)
        IF (Present(Open)) THEN
            Reset DO_CLOSE_GRP;
            Set DO_OPEN_GRP;
            IF (NOT SIM_ON) WaitDI DI_GRP_APERTA, 1;
            IF (SIM_ON) WaitTime 0.5;
        ELSEIF (Present(Close)) THEN
            Reset DO_OPEN_GRP;
            Set DO_CLOSE_GRP;
            IF (NOT SIM_ON) WaitDI DI_GRP_CHIUSA, 1;
            IF (SIM_ON) WaitTime 0.5;
        ENDIF
        RETURN;
    ENDPROC
    
    PROC initSignal()
        ! Collegamento dei segnali alias a segnali inseriti nella configurazione.
        ! Questa procedura è necessaria solo se si utilizzano segnali definiti come dati signaldi / signaldo. 
        ! In questo caso ho usato questo metodo in modo da rendere caricabile senza errori il modulo. 
        !
        ! Personalmente sconsiglio questo sistema per gestire gli I/O in quanto necessita di una configurazione
        ! aggiuntiva e risulta essere più delicata e a rischio errori
        !
!        AliasIO di_00, DI_REQ_HOME;
!        AliasIO di_01, DI_PRELIEVO;
!        AliasIO di_02, DI_DEPOSITO;
!        AliasIO di_03, DI_GRP_APERTA;
!        AliasIO di_04, DI_GRP_CHIUSA;
!        
!        AliasIO do_00, DO_ROB_HOME;
!        AliasIO do_01, DO_PRELIEVO_OK;
!        AliasIO do_02, DO_DEPOSITO_OK;
!        AliasIO do_03, DO_OPEN_GRP;
!        AliasIO do_04, DO_CLOSE_GRP;
        RETURN;
    ENDPROC
ENDMODULE
