MODULE rapid_overview
    !
    ! ************************************************************************
    ! * Programma esempio di panoramica di rapid
    ! * 
    ! * Autore: William Guerra <william.guerra@mr-robotica.it>
    ! * Ditta : MR Robotica
    ! *
    ! * Link  : https://www.bitbucket.org/william_guerra/presentazione_yumi.git
    ! ************************************************************************
    !
    ! Questo modulo vuole essere una panoramica di RAPID, di seguito troverete pertanto una serie di defininizoni delle
    ! variabili maggiormente usate nella programmazione e una serie di esempi su come definire le strutture fondamentali
    ! di RAPID
    ! 
    ! Iniziamo con un po' di definizioni di dati.
    ! In un modulo RAPID i dati globali, vanno definiti all'inizio del modulo.
    !  
    ! La definizione segue il modello: 
    !   [<visibilità>] <Attributi> <tipo di dato> <nome del dato> [:= <valore di inizializzazione>];
    !
    ! <Visibilità>: definisce la visibilità del dato, può essere omesso
    !   TASK  Il dato è visibile solo nel task corrente
    !   LOCAL Il dato è visibile solo nel modulo corrente
    !
    ! <Attributi>: definisce la persistenza del dato:
    !   PERS  Dato persistente, il valore rimane in memoria anche al riavvio del controller o al cambio programma
    !   CONST Dato di tipo costante, il valore rimane in memoria come per i dati PERS ma non è modificabile dal programma
    !   VAR   Dato di tipo variabile, il valore non viene memorizzato il dato viene perso al riavvio o cambio programma
    !
    ! <tipo di variabile>: definisce il tipo di dato
    !
    ! <valore di inizializzazione>: opzionale per dati variabili, obbligatorio per gli altri, inizializza il dato con il
    !                               valore dopo l'operatore di assegnazione
    !
    PERS num numero := 1;

    PERS bool boolean := TRUE;

    PERS string stringa := "Hello World";

    PERS tooldata myTool := [TRUE,[[0,0,1],[1,0,0,0]],[0.1,[0,0,1],[1,0,0,0],0,0,0]];

    CONST robtarget myRobtarget:=[[100,200,300],[1,0,0,0],[0,0,0,0],[9E9,9E9,9E9,9E9,9E9,9E9]];

    TASK PERS wobjdata myWobj:=[FALSE,TRUE,"",[[0,0,0],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];

    !
    ! Terminata la definizione delle variabili è possibile definire routine trap, procedure e funzioni, ognuna di esse
    ! verrà definita come:
    !
    !   PROC myProc(<parametri opzionali>)
    !     Definizone dei dati locali
    !      
    !     Codice
    !     [RETURN;]
    !   ENDPROC
    !
    !   FUNC <tipo di ritorno> myFunc(<parametri opzionali>)
    !     Definizione dei dati locali
    !
    !     Codice
    !     RETURN <valore di ritorno>;
    !   ENDFUNC
    !
    !   TRAP myTrap
    !     Definizione dei dati locali
    !
    !     Codice
    !   ENDTRAP
    !
    ! In ogni task non sono ammesse routine con nomi uguali, notare che RAPID è case insensitive (purtoppo) e pertanto NON
    ! FA DISTINZIONE TRA MAIUSCOLE E MINUSCOLE.
    ! 
    ! E' necessario definire anche una E UNA SOLA procedura main da cui il programma inizierà. Tale procedure DEVE
    ! ESSERE definita nel modo seguente:
    PROC main()
        ! Definizione di variabili locali
        VAR num dato := 42;
        VAR num answer;
        !
        ! Codice
        !
        ! Esempio di chiamata a procedura con passaggio dati
        myProc dato;
        !
        ! Esempio di chiamata a funzione con passaggio dati
        answer := myFunc(dato);
    ENDPROC

    PROC myProc(num numero)
        ! Definizione di variabili locali
        !
        ! Codice
    ENDPROC

    FUNC num myFunc(num numero)
        ! Definizione di dati locali
        !
        ! Codice
        RETURN numero;
    ENDFUNC
ENDMODULE
